FROM node:latest as node

WORKDIR /app

# Copy the package.json and package-lock.json files to the container
COPY package*.json ./

# Install the app's dependencies
RUN npm install -g @angular/cli
RUN npm install --save-dev @angular-devkit/build-angular

COPY .env /usr/src/app

# Copy the rest of the app's files to the container
COPY . .

# Build the Angular app
RUN ng build

# Expose port 4200
EXPOSE 4200

# Start the Angular app
CMD ["npm", "start"]